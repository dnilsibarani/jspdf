import { AlignmentType, BorderStyle, Paragraph, Spacing, TabStopPosition, TabStopType, Table, TableCell, TableRow, TextRun, UnderlineType, WidthType } from "docx"

const bodyValue = 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Officia dolorem laboriosam, ab fugit iure repellendus molestiae doloremque commodi corrupti unde obcaecati saepe, pariatur itaque. Consequuntur at aut velit doloribus esse.'

const biodata = {
    name : 'AMATE ADMIN OTP',
    pangkat: 'IV/c',
    nip: '76544321',
    jabatan: 'Kepala Seksi Intelijen'
}


function titleSection() {
    return new Table({
        width: {
            size: 100,
            type: WidthType.PERCENTAGE
        },
        rows: [
            new TableRow({
                children: [
                    new TableCell({
                        borders: {
                            top: { style: BorderStyle.NONE, size: 0, color: "ff0000" },
                            bottom: { style: BorderStyle.NONE, size: 0, color: "ff0000" },
                            left: { style: BorderStyle.NONE, size: 0, color: "ff0000" },
                            right: { style: BorderStyle.NONE, size: 0, color: "ff0000" },
                        },
                        children: [
                            new Paragraph({
                                children: [
                                    new TextRun({
                                        text: 'KEJAKSAAN AGUNG R.I',
                                        size: 22,
                                        bold: true,
                                        underline: {
                                            type: UnderlineType.SINGLE,
                                            color: "#000000",
                                        },
                                    })
                                ]
                            })
                        ]
                    }),
                    new TableCell({
                        borders: {
                            top: { style: BorderStyle.NONE, size: 0, color: "ff0000" },
                            bottom: { style: BorderStyle.NONE, size: 0, color: "ff0000" },
                            left: { style: BorderStyle.NONE, size: 0, color: "ff0000" },
                            right: { style: BorderStyle.NONE, size: 0, color: "ff0000" },
                        },
                        children: [
                            new Paragraph({
                                alignment: AlignmentType.RIGHT,
                                children: [
                                    new TextRun({
                                        text: 'IN.1',
                                        size: 22
                                    })
                                ]
                            }),
                            new Paragraph({
                                spacing: {
                                    after: 10
                                }
                            }),
                            new Paragraph({
                                alignment: AlignmentType.RIGHT,
                                children: [
                                    new TextRun({
                                        text: `COPY KE : 1`,
                                        size: 22
                                    })
                                ]
                            }),
                            new Paragraph({
                                alignment: AlignmentType.RIGHT,
                                children: [
                                    new TextRun({
                                        text: `DARI 50 COPIES`,
                                        size: 22
                                    })
                                ]
                            }),
                        ]
                    })

                ]
            }),
        ]
    })
}

function kopSection() {
    return [
        new Paragraph({
            alignment: AlignmentType.CENTER,
            children: [
                new TextRun({
                    text: 'SURAT PERINTAH TUGAS',
                    size: 22,
                    bold: true
                })
            ]
        }),
        new Paragraph({
            alignment: AlignmentType.CENTER,
            children: [
                new TextRun({
                    text: "------------------------------------------------------------",
                    size: 22,
                })
            ]
        }),
        new Paragraph({
            alignment: AlignmentType.CENTER,
            children: [
                new TextRun({
                    text: 'No : SP.TUG-13/KAR/[kodemasalah]/2/2023 ',
                    size: 22,
                })
            ]
        }),
    ]
}

function bodyMenimbang (){
    return new Table({
        width: {
            size: 100,
            type: WidthType.PERCENTAGE
        },
        
        rows: [
            new TableRow({
                children: [
                    new TableCell({
                        borders: {
                            top: { style: BorderStyle.NONE, size: 0, color: "ff0000" },
                            bottom: { style: BorderStyle.NONE, size: 0, color: "ff0000" },
                            left: { style: BorderStyle.NONE, size: 0, color: "ff0000" },
                            right: { style: BorderStyle.NONE, size: 0, color: "ff0000" },
                        },
                        width: {
                            size: 22,
                            type: WidthType.PERCENTAGE
                        },
                        children: [
                            new Paragraph({
                                children: [
                                    new TextRun({
                                        text: 'Menimbang',
                                        size: 22,
                                    })
                                ]
                            })
                        ]
                    }),
                    new TableCell({
                        borders: {
                            top: { style: BorderStyle.NONE, size: 0, color: "ff0000" },
                            bottom: { style: BorderStyle.NONE, size: 0, color: "ff0000" },
                            left: { style: BorderStyle.NONE, size: 0, color: "ff0000" },
                            right: { style: BorderStyle.NONE, size: 0, color: "ff0000" },
                        },
                        width: {
                            size: 10,
                            type: WidthType.PERCENTAGE
                        },
                        children: [
                            new Paragraph({
                                // alignment: AlignmentType.RIGHT,
                                children: [
                                    new TextRun({
                                        text: `: 1.`,
                                        size: 22
                                        
                                    })
                                ]
                            }),
                        ]
                    }),
                    new TableCell({
                        borders: {
                            top: { style: BorderStyle.NONE, size: 0, color: "ff0000" },
                            bottom: { style: BorderStyle.NONE, size: 0, color: "ff0000" },
                            left: { style: BorderStyle.NONE, size: 0, color: "ff0000" },
                            right: { style: BorderStyle.NONE, size: 0, color: "ff0000" },
                        },
                        children: [
                            new Paragraph({
                                // alignment: AlignmentType.RIGHT,
                                children: [
                                    new TextRun({
                                        text: `${bodyValue}`,
                                        numbering: {
                                            reference: "my-crazy-numbering",
                                            level: 1,
                                        },
                                    })
                                ]
                            }),
                        ]
                    })

                ]
            }),
        ]
    })
} 

function bodyDasar (){
    return new Table({
        width: {
            size: 100,
            type: WidthType.PERCENTAGE
        },
        
        rows: [
            new TableRow({
                children: [
                    new TableCell({
                        borders: {
                            top: { style: BorderStyle.NONE, size: 0, color: "ff0000" },
                            bottom: { style: BorderStyle.NONE, size: 0, color: "ff0000" },
                            left: { style: BorderStyle.NONE, size: 0, color: "ff0000" },
                            right: { style: BorderStyle.NONE, size: 0, color: "ff0000" },
                        },
                        width: {
                            size: 22,
                            type: WidthType.PERCENTAGE
                        },
                        children: [
                            new Paragraph({
                                children: [
                                    new TextRun({
                                        text: 'Dasar',
                                        size: 22,
                                    })
                                ]
                            })
                        ]
                    }),
                    new TableCell({
                        borders: {
                            top: { style: BorderStyle.NONE, size: 0, color: "ff0000" },
                            bottom: { style: BorderStyle.NONE, size: 0, color: "ff0000" },
                            left: { style: BorderStyle.NONE, size: 0, color: "ff0000" },
                            right: { style: BorderStyle.NONE, size: 0, color: "ff0000" },
                        },
                        width: {
                            size: 10,
                            type: WidthType.PERCENTAGE
                        },
                        children: [
                            new Paragraph({
                                // alignment: AlignmentType.RIGHT,
                                children: [
                                    new TextRun({
                                        text: `: 1.`,
                                        size: 22
                                        
                                    })
                                ]
                            }),
                        ]
                    }),
                    new TableCell({
                        borders: {
                            top: { style: BorderStyle.NONE, size: 0, color: "ff0000" },
                            bottom: { style: BorderStyle.NONE, size: 0, color: "ff0000" },
                            left: { style: BorderStyle.NONE, size: 0, color: "ff0000" },
                            right: { style: BorderStyle.NONE, size: 0, color: "ff0000" },
                        },
                        children: [
                            new Paragraph({
                                // alignment: AlignmentType.RIGHT,
                                children: [
                                    new TextRun({
                                        text: `${bodyValue}`,
                                        numbering: {
                                            reference: "my-crazy-numbering",
                                            level: 1,
                                        },
                                    })
                                ]
                            }),
                        ]
                    })

                ]
            }),
        ]
    })
}

function bodyMemerintahkan (){
    return [
        new Paragraph({
            alignment: AlignmentType.CENTER,
            children: [
                new TextRun({
                    text: 'MEMERINTAHKAN',
                    size: 22,
                }),
            ],
            spacing: {
                after: 200
            }
        }),
        new Table({
            width: {
                size: 100,
                type: WidthType.PERCENTAGE
            },
            
            rows: [
                new TableRow({
                    children: [
                        new TableCell({
                            borders: {
                                top: { style: BorderStyle.NONE, size: 0, color: "ff0000" },
                                bottom: { style: BorderStyle.NONE, size: 0, color: "ff0000" },
                                left: { style: BorderStyle.NONE, size: 0, color: "ff0000" },
                                right: { style: BorderStyle.NONE, size: 0, color: "ff0000" },
                            },
                            width: {
                                size: 22,
                                type: WidthType.PERCENTAGE
                            },
                            children: [
                                new Paragraph({
                                    children: [
                                        new TextRun({
                                            text: 'Kepada',
                                            size: 22,
                                        })
                                    ]
                                })
                            ]
                        }),
                        new TableCell({
                            borders: {
                                top: { style: BorderStyle.NONE, size: 0, color: "ff0000" },
                                bottom: { style: BorderStyle.NONE, size: 0, color: "ff0000" },
                                left: { style: BorderStyle.NONE, size: 0, color: "ff0000" },
                                right: { style: BorderStyle.NONE, size: 0, color: "ff0000" },
                            },
                            width: {
                                size: 10,
                                type: WidthType.PERCENTAGE
                            },
                            children: [
                                new Paragraph({
                                    children: [
                                        new TextRun({
                                            text: `: 1.`,
                                            size: 22
                                            
                                        })
                                    ]
                                }),
                            ]
                        }),
                        new TableCell({
                            borders: {
                                top: { style: BorderStyle.NONE, size: 0, color: "ff0000" },
                                bottom: { style: BorderStyle.NONE, size: 0, color: "ff0000" },
                                left: { style: BorderStyle.NONE, size: 0, color: "ff0000" },
                                right: { style: BorderStyle.NONE, size: 0, color: "ff0000" },
                            },
                            children: [
                                new Paragraph({
                                        children :[
                                            new TextRun ({
                                                text:'Nama',
                                                size: 22
                                            })
                                        ]
                                    })
                            ]
                        }),
                        new TableCell({
                            borders: {
                                top: { style: BorderStyle.NONE, size: 0, color: "ff0000" },
                                bottom: { style: BorderStyle.NONE, size: 0, color: "ff0000" },
                                left: { style: BorderStyle.NONE, size: 0, color: "ff0000" },
                                right: { style: BorderStyle.NONE, size: 0, color: "ff0000" },
                            },
                            children: [
                                new Paragraph({
                                    children: [
                                        new TextRun({
                                            text: `: ${biodata.name}`,
                                            numbering: {
                                                reference: "my-crazy-numbering",
                                                level: 1,
                                            },
                                        })
                                    ]
                                }),
                            ]
                        })
    
                    ]
                }),
            ]
        }),
        new Table({
            width: {
                size: 100,
                type: WidthType.PERCENTAGE
            },
            
            rows: [
                new TableRow({
                    children: [
                        new TableCell({
                            borders: {
                                top: { style: BorderStyle.NONE, size: 0, color: "ff0000" },
                                bottom: { style: BorderStyle.NONE, size: 0, color: "ff0000" },
                                left: { style: BorderStyle.NONE, size: 0, color: "ff0000" },
                                right: { style: BorderStyle.NONE, size: 0, color: "ff0000" },
                            },
                            width: {
                                size: 22,
                                type: WidthType.PERCENTAGE
                            },
                            children: [
                                new Paragraph({
                                    children: [
                                        new TextRun({
                                            text: '',
                                            size: 22,
                                        })
                                    ]
                                })
                            ]
                        }),
                        new TableCell({
                            borders: {
                                top: { style: BorderStyle.NONE, size: 0, color: "ff0000" },
                                bottom: { style: BorderStyle.NONE, size: 0, color: "ff0000" },
                                left: { style: BorderStyle.NONE, size: 0, color: "ff0000" },
                                right: { style: BorderStyle.NONE, size: 0, color: "ff0000" },
                            },
                            width: {
                                size: 10,
                                type: WidthType.PERCENTAGE
                            },
                            children: [
                                new Paragraph({
                                    children: [
                                        new TextRun({
                                            text: ``,
                                            size: 22
                                            
                                        })
                                    ]
                                }),
                            ]
                        }),
                        new TableCell({
                            borders: {
                                top: { style: BorderStyle.NONE, size: 0, color: "ff0000" },
                                bottom: { style: BorderStyle.NONE, size: 0, color: "ff0000" },
                                left: { style: BorderStyle.NONE, size: 0, color: "ff0000" },
                                right: { style: BorderStyle.NONE, size: 0, color: "ff0000" },
                            },
                            children: [
                                new Paragraph({
                                        children :[
                                            new TextRun ({
                                                text:'Pangkat',
                                                size: 22
                                            })
                                        ]
                                    })
                            ]
                        }),
                        new TableCell({
                            borders: {
                                top: { style: BorderStyle.NONE, size: 0, color: "ff0000" },
                                bottom: { style: BorderStyle.NONE, size: 0, color: "ff0000" },
                                left: { style: BorderStyle.NONE, size: 0, color: "ff0000" },
                                right: { style: BorderStyle.NONE, size: 0, color: "ff0000" },
                            },
                            children: [
                                new Paragraph({
                                    children: [
                                        new TextRun({
                                            text: `: ${biodata.pangkat}`,
                                            numbering: {
                                                reference: "my-crazy-numbering",
                                                level: 1,
                                            },
                                        })
                                    ]
                                }),
                            ]
                        })
    
                    ]
                }),
            ]
        }),
        new Table({
            width: {
                size: 100,
                type: WidthType.PERCENTAGE
            },
            
            rows: [
                new TableRow({
                    children: [
                        new TableCell({
                            borders: {
                                top: { style: BorderStyle.NONE, size: 0, color: "ff0000" },
                                bottom: { style: BorderStyle.NONE, size: 0, color: "ff0000" },
                                left: { style: BorderStyle.NONE, size: 0, color: "ff0000" },
                                right: { style: BorderStyle.NONE, size: 0, color: "ff0000" },
                            },
                            width: {
                                size: 22,
                                type: WidthType.PERCENTAGE
                            },
                            children: [
                                new Paragraph({
                                    children: [
                                        new TextRun({
                                            text: '',
                                            size: 22,
                                        })
                                    ]
                                })
                            ]
                        }),
                        new TableCell({
                            borders: {
                                top: { style: BorderStyle.NONE, size: 0, color: "ff0000" },
                                bottom: { style: BorderStyle.NONE, size: 0, color: "ff0000" },
                                left: { style: BorderStyle.NONE, size: 0, color: "ff0000" },
                                right: { style: BorderStyle.NONE, size: 0, color: "ff0000" },
                            },
                            width: {
                                size: 10,
                                type: WidthType.PERCENTAGE
                            },
                            children: [
                                new Paragraph({
                                    children: [
                                        new TextRun({
                                            text: ``,
                                            size: 22
                                            
                                        })
                                    ]
                                }),
                            ]
                        }),
                        new TableCell({
                            borders: {
                                top: { style: BorderStyle.NONE, size: 0, color: "ff0000" },
                                bottom: { style: BorderStyle.NONE, size: 0, color: "ff0000" },
                                left: { style: BorderStyle.NONE, size: 0, color: "ff0000" },
                                right: { style: BorderStyle.NONE, size: 0, color: "ff0000" },
                            },
                            children: [
                                new Paragraph({
                                        children :[
                                            new TextRun ({
                                                text:'Jabatan',
                                                size: 22
                                            })
                                        ]
                                    })
                            ]
                        }),
                        new TableCell({
                            borders: {
                                top: { style: BorderStyle.NONE, size: 0, color: "ff0000" },
                                bottom: { style: BorderStyle.NONE, size: 0, color: "ff0000" },
                                left: { style: BorderStyle.NONE, size: 0, color: "ff0000" },
                                right: { style: BorderStyle.NONE, size: 0, color: "ff0000" },
                            },
                            children: [
                                new Paragraph({
                                    children: [
                                        new TextRun({
                                            text: `: ${biodata.jabatan}`,
                                            numbering: {
                                                reference: "my-crazy-numbering",
                                                level: 1,
                                            },
                                        })
                                    ]
                                }),
                            ]
                        })
    
                    ]
                }),
            ]
        }),
        new Table({
            width: {
                size: 100,
                type: WidthType.PERCENTAGE
            },
            
            rows: [
                new TableRow({
                    children: [
                        new TableCell({
                            borders: {
                                top: { style: BorderStyle.NONE, size: 0, color: "ff0000" },
                                bottom: { style: BorderStyle.NONE, size: 0, color: "ff0000" },
                                left: { style: BorderStyle.NONE, size: 0, color: "ff0000" },
                                right: { style: BorderStyle.NONE, size: 0, color: "ff0000" },
                            },
                            width: {
                                size: 22,
                                type: WidthType.PERCENTAGE
                            },
                            children: [
                                new Paragraph({
                                    children: [
                                        new TextRun({
                                            text: '',
                                            size: 22,
                                        })
                                    ]
                                })
                            ]
                        }),
                        new TableCell({
                            borders: {
                                top: { style: BorderStyle.NONE, size: 0, color: "ff0000" },
                                bottom: { style: BorderStyle.NONE, size: 0, color: "ff0000" },
                                left: { style: BorderStyle.NONE, size: 0, color: "ff0000" },
                                right: { style: BorderStyle.NONE, size: 0, color: "ff0000" },
                            },
                            width: {
                                size: 10,
                                type: WidthType.PERCENTAGE
                            },
                            children: [
                                new Paragraph({
                                    children: [
                                        new TextRun({
                                            text: ``,
                                            size: 22
                                            
                                        })
                                    ]
                                }),
                            ]
                        }),
                        new TableCell({
                            borders: {
                                top: { style: BorderStyle.NONE, size: 0, color: "ff0000" },
                                bottom: { style: BorderStyle.NONE, size: 0, color: "ff0000" },
                                left: { style: BorderStyle.NONE, size: 0, color: "ff0000" },
                                right: { style: BorderStyle.NONE, size: 0, color: "ff0000" },
                            },
                            children: [
                                new Paragraph({
                                        children :[
                                            new TextRun ({
                                                text:'NIP',
                                                size: 22
                                            })
                                        ]
                                    })
                            ]
                        }),
                        new TableCell({
                            borders: {
                                top: { style: BorderStyle.NONE, size: 0, color: "ff0000" },
                                bottom: { style: BorderStyle.NONE, size: 0, color: "ff0000" },
                                left: { style: BorderStyle.NONE, size: 0, color: "ff0000" },
                                right: { style: BorderStyle.NONE, size: 0, color: "ff0000" },
                            },
                            children: [
                                new Paragraph({
                                    children: [
                                        new TextRun({
                                            text: `: ${biodata.nip}`,
                                            numbering: {
                                                reference: "my-crazy-numbering",
                                                level: 1,
                                            },
                                        })
                                    ]
                                }),
                            ]
                        })
    
                    ]
                }),
            ]
        }),
    ]
}

function bodyUntuk (){
    return new Table({
        width: {
            size: 100,
            type: WidthType.PERCENTAGE
        },
        
        rows: [
            new TableRow({
                children: [
                    new TableCell({
                        borders: {
                            top: { style: BorderStyle.NONE, size: 0, color: "ff0000" },
                            bottom: { style: BorderStyle.NONE, size: 0, color: "ff0000" },
                            left: { style: BorderStyle.NONE, size: 0, color: "ff0000" },
                            right: { style: BorderStyle.NONE, size: 0, color: "ff0000" },
                        },
                        width: {
                            size: 22,
                            type: WidthType.PERCENTAGE
                        },
                        children: [
                            new Paragraph({
                                children: [
                                    new TextRun({
                                        text: 'Untuk',
                                        size: 22,
                                    })
                                ]
                            })
                        ]
                    }),
                    new TableCell({
                        borders: {
                            top: { style: BorderStyle.NONE, size: 0, color: "ff0000" },
                            bottom: { style: BorderStyle.NONE, size: 0, color: "ff0000" },
                            left: { style: BorderStyle.NONE, size: 0, color: "ff0000" },
                            right: { style: BorderStyle.NONE, size: 0, color: "ff0000" },
                        },
                        width: {
                            size: 10,
                            type: WidthType.PERCENTAGE
                        },
                        children: [
                            new Paragraph({
                                // alignment: AlignmentType.RIGHT,
                                children: [
                                    new TextRun({
                                        text: `: 1.`,
                                        size: 22
                                        
                                    })
                                ]
                            }),
                        ]
                    }),
                    new TableCell({
                        borders: {
                            top: { style: BorderStyle.NONE, size: 0, color: "ff0000" },
                            bottom: { style: BorderStyle.NONE, size: 0, color: "ff0000" },
                            left: { style: BorderStyle.NONE, size: 0, color: "ff0000" },
                            right: { style: BorderStyle.NONE, size: 0, color: "ff0000" },
                        },
                        children: [
                            new Paragraph({
                                // alignment: AlignmentType.RIGHT,
                                children: [
                                    new TextRun({
                                        text: `${bodyValue}`,
                                        numbering: {
                                            reference: "my-crazy-numbering",
                                            level: 1,
                                        },
                                    })
                                ]
                            }),
                        ]
                    })

                ]
            }),
        ]
    })
}

function dikeluarkanTempat (){
    return new Table({
        width:{
            size:100,
            type:WidthType.PERCENTAGE
        },
        rows:[
            new TableRow({
                children : [
                    new TableCell({
                        borders: {
                            top: { style: BorderStyle.NONE, size: 0, color: "ff0000" },
                            bottom: { style: BorderStyle.NONE, size: 0, color: "ff0000" },
                            left: { style: BorderStyle.NONE, size: 0, color: "ff0000" },
                            right: { style: BorderStyle.NONE, size: 0, color: "ff0000" },
                        },
                        width: {
                            size: 20,
                            type: WidthType.PERCENTAGE
                        },
                        children: [
                            new Paragraph({
                                // alignment: AlignmentType.RIGHT,
                                children: [
                                    new TextRun({
                                        size:22
                                    })
                                ]
                            })
                        ]
                    }),
                    new TableCell({
                        borders: {
                            top: { style: BorderStyle.NONE, size: 0, color: "ff0000" },
                            bottom: { style: BorderStyle.NONE, size: 0, color: "ff0000" },
                            left: { style: BorderStyle.NONE, size: 0, color: "ff0000" },
                            right: { style: BorderStyle.NONE, size: 0, color: "ff0000" },
                        },
                        // width: {
                        //     size: 22,
                        //     type: WidthType.PERCENTAGE
                        // },
                        children: [
                            new Paragraph({
                                alignment: AlignmentType.RIGHT,
                                children: [
                                    new TextRun({
                                        text: 'Dikeluarkan di',
                                        size: 22,
                                    })
                                ]
                            })
                        ]
                    }),
                    new TableCell({
                        borders: {
                            top: { style: BorderStyle.NONE, size: 0, color: "ff0000" },
                            bottom: { style: BorderStyle.NONE, size: 0, color: "ff0000" },
                            left: { style: BorderStyle.NONE, size: 0, color: "ff0000" },
                            right: { style: BorderStyle.NONE, size: 0, color: "ff0000" },
                        },
                        children: [
                            new Paragraph({
                                alignment: AlignmentType.CENTER,
                                children: [
                                    new TextRun({
                                        text: ': Jakarta',
                                        size: 22,
                                    })
                                ]
                            })
                        ]
                    }),
                ]
            })
        ]
    })
}

function dikeluarkanTanggal (){
    return new Table({
        width:{
            size:100,
            type:WidthType.PERCENTAGE
        },
        rows:[
            new TableRow({
                children : [
                    new TableCell({
                        borders: {
                            top: { style: BorderStyle.NONE, size: 0, color: "ff0000" },
                            bottom: { style: BorderStyle.NONE, size: 0, color: "ff0000" },
                            left: { style: BorderStyle.NONE, size: 0, color: "ff0000" },
                            right: { style: BorderStyle.NONE, size: 0, color: "ff0000" },
                        },
                        children: [
                            new Paragraph({
                                alignment: AlignmentType.RIGHT,
                                children: [
                                    new TextRun({
                                        size:22
                                    })
                                ]
                            })
                        ],
                    }),
                    new TableCell({
                        borders: {
                            top: { style: BorderStyle.NONE, size: 0, color: "ff0000" },
                            bottom: { style: BorderStyle.NONE, size: 0, color: "ff0000" },
                            left: { style: BorderStyle.NONE, size: 0, color: "ff0000" },
                            right: { style: BorderStyle.NONE, size: 0, color: "ff0000" },
                        },
                        width: {
                            size: 56,
                            type: WidthType.PERCENTAGE
                        },
                        children: [
                            new Paragraph({
                                alignment: AlignmentType.RIGHT,
                                children: []
                            })
                        ],
                        
                    }),
                    new TableCell({
                        borders: {
                            top: { style: BorderStyle.NONE, size: 0, color: "ff0000" },
                            bottom: { style: BorderStyle.NONE, size: 0, color: "ff0000" },
                            left: { style: BorderStyle.NONE, size: 0, color: "ff0000" },
                            right: { style: BorderStyle.NONE, size: 0, color: "ff0000" },
                        },
                        width: {
                            size: 21,
                            type: WidthType.PERCENTAGE
                        },
                        children: [
                            new Paragraph({
                                children: [
                                    new TextRun({
                                        text: 'Pada Tanggal ',
                                        size: 22,
                                    })
                                ]
                            })
                        ],
                        
                    }),
                    new TableCell({
                        borders: {
                            top: { style: BorderStyle.NONE, size: 0, color: "ff0000" },
                            bottom: { style: BorderStyle.NONE, size: 0, color: "ff0000" },
                            left: { style: BorderStyle.NONE, size: 0, color: "ff0000" },
                            right: { style: BorderStyle.NONE, size: 0, color: "ff0000" },
                        },
                        children: [
                            new Paragraph({
                                alignment: AlignmentType.CENTER,
                                children: [
                                    new TextRun({
                                        text: ': 25 April 2021',
                                        size: 22,
                                    })
                                ]
                            })
                        ]
                    }),
                ]
            })
        ]
    })
}

function tandaTangan (){
    return[
        new Table({
            width: {
                size: 100,
                type: WidthType.PERCENTAGE
            },
            rows: [
                new TableRow({
                    children: [
                        new TableCell({
                            borders: {
                                top: { style: BorderStyle.NONE, size: 0, color: "ff0000" },
                                bottom: { style: BorderStyle.NONE, size: 0, color: "ff0000" },
                                left: { style: BorderStyle.NONE, size: 0, color: "ff0000" },
                                right: { style: BorderStyle.NONE, size: 0, color: "ff0000" },
                            },
                            width: {
                                size: 50,
                                type: WidthType.PERCENTAGE
                            },
                            children: []
                        }),
                        new TableCell({
                            borders: {
                                top: { style: BorderStyle.NONE, size: 0, color: "ff0000" },
                                bottom: { style: BorderStyle.NONE, size: 0, color: "ff0000" },
                                left: { style: BorderStyle.NONE, size: 0, color: "ff0000" },
                                right: { style: BorderStyle.NONE, size: 0, color: "ff0000" },
                            },
                            children: [
                                new Paragraph({
                                    alignment: AlignmentType.CENTER,
                                    children: [
                                        new TextRun({
                                            text: 'KEPALA SEKSI INTELIJEN',
                                            size: 22
                                        })
                                    ]
                                })
                            ]
                        }),
                    ]
                }),
            ]
        }),
        new Paragraph({
            spacing: {
                after: 600
            }
        }),
        new Table({
            width: {
                size: 100,
                type: WidthType.PERCENTAGE
            },
            rows: [
                new TableRow({
                    children: [
                        new TableCell({
                            borders: {
                                top: { style: BorderStyle.NONE, size: 0, color: "ff0000" },
                                bottom: { style: BorderStyle.NONE, size: 0, color: "ff0000" },
                                left: { style: BorderStyle.NONE, size: 0, color: "ff0000" },
                                right: { style: BorderStyle.NONE, size: 0, color: "ff0000" },
                            },
                            width: {
                                size: 50,
                                type: WidthType.PERCENTAGE
                            },
                            children: []
                        }),
                        new TableCell({
                            borders: {
                                top: { style: BorderStyle.NONE, size: 0, color: "ff0000" },
                                bottom: { style: BorderStyle.NONE, size: 0, color: "ff0000" },
                                left: { style: BorderStyle.NONE, size: 0, color: "ff0000" },
                                right: { style: BorderStyle.NONE, size: 0, color: "ff0000" },
                            },
                            children: [
                                new Paragraph({
                                    alignment: AlignmentType.CENTER,
                                    children: [
                                        new TextRun({
                                            text:'AMATE ADMIN OTP',
                                            size: 22,
                                            underline: {
                                                type: UnderlineType.SINGLE,
                                                color: "000000"
                                            }
                                        })
                                    ]
                                })
                            ]
                        }),
                    ]
                }),
                new TableRow({
                    children: [
                        new TableCell({
                            borders: {
                                top: { style: BorderStyle.NONE, size: 0, color: "ff0000" },
                                bottom: { style: BorderStyle.NONE, size: 0, color: "ff0000" },
                                left: { style: BorderStyle.NONE, size: 0, color: "ff0000" },
                                right: { style: BorderStyle.NONE, size: 0, color: "ff0000" },
                            },
                            width: {
                                size: 50,
                                type: WidthType.PERCENTAGE
                            },
                            children: []
                        }),
                        new TableCell({
                            borders: {
                                top: { style: BorderStyle.NONE, size: 0, color: "ff0000" },
                                bottom: { style: BorderStyle.NONE, size: 0, color: "ff0000" },
                                left: { style: BorderStyle.NONE, size: 0, color: "ff0000" },
                                right: { style: BorderStyle.NONE, size: 0, color: "ff0000" },
                            },
                            children: [
                                new Paragraph({
                                    alignment: AlignmentType.CENTER,
                                    children: [
                                        new TextRun({
                                            text: `IV/c Pembina Utama Muda`,
                                            size: 22
                                        })
                                    ]
                                })
                            ]
                        }),
                    ]
                }),
                new TableRow({
                    children: [
                        new TableCell({
                            borders: {
                                top: { style: BorderStyle.NONE, size: 0, color: "ff0000" },
                                bottom: { style: BorderStyle.NONE, size: 0, color: "ff0000" },
                                left: { style: BorderStyle.NONE, size: 0, color: "ff0000" },
                                right: { style: BorderStyle.NONE, size: 0, color: "ff0000" },
                            },
                            width: {
                                size: 50,
                                type: WidthType.PERCENTAGE
                            },
                            children: []
                        }),
                        new TableCell({
                            borders: {
                                top: { style: BorderStyle.NONE, size: 0, color: "ff0000" },
                                bottom: { style: BorderStyle.NONE, size: 0, color: "ff0000" },
                                left: { style: BorderStyle.NONE, size: 0, color: "ff0000" },
                                right: { style: BorderStyle.NONE, size: 0, color: "ff0000" },
                            },
                            children: [
                                new Paragraph({
                                    alignment: AlignmentType.CENTER,
                                    children: [
                                        new TextRun({
                                            text: `Nip:76554432`,
                                            size: 22
                                        })
                                    ]
                                })
                            ]
                        }),
                    ]
                }),
            ]
        })
    ]
}

function tembusan (){
    return new Table({
        width: {
            size: 100,
            type: WidthType.PERCENTAGE
        },
        rows: [
            new TableRow({
                children: [
                    new TableCell({
                        borders: {
                            top: { style: BorderStyle.NONE, size: 0, color: "ff0000" },
                            bottom: { style: BorderStyle.NONE, size: 0, color: "ff0000" },
                            left: { style: BorderStyle.NONE, size: 0, color: "ff0000" },
                            right: { style: BorderStyle.NONE, size: 0, color: "ff0000" },
                        },
                        children: [
                            new Paragraph({
                                alignment: AlignmentType.LEFT,
                                children: [
                                    new TextRun({
                                        text: `Tembusan`,
                                        size: 22,
                                    })
                                ]
                            })
                        ]
                    })
                ]
            }),
            new TableRow({
                children: [
                    new TableCell({
                        borders: {
                            top: { style: BorderStyle.NONE, size: 0, color: "ff0000" },
                            bottom: { style: BorderStyle.NONE, size: 0, color: "ff0000" },
                            left: { style: BorderStyle.NONE, size: 0, color: "ff0000" },
                            right: { style: BorderStyle.NONE, size: 0, color: "ff0000" },
                        },
                        children: [
                            new Paragraph({
                                alignment: AlignmentType.LEFT,
                                children: [
                                    new TextRun({
                                        text: `123`,
                                        size: 22,
                                    })
                                ]
                            })
                        ]
                    })
                ]
            }),
        ]
    })
}

export {titleSection, kopSection, bodyMenimbang,bodyDasar,bodyMemerintahkan,bodyUntuk,dikeluarkanTempat,dikeluarkanTanggal,tandaTangan,tembusan, bodyValue}