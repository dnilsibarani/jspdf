import './App.css';
import 'bootstrap/dist/css/bootstrap.min.css';
import PrintDocument from './Document';

function App() {
  return (
    <>
    <PrintDocument/>
    </>
  );
}

export default App;
