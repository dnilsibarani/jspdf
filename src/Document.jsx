import React from 'react'
import { jsPDF } from "jspdf";
import { useRef } from 'react'
import {Document, Packer, Paragraph, HeadingLevel, AlignmentType, TextRun, Header } from 'docx'
import { saveAs } from "file-saver";
import { bodyDasar, bodyMemerintahkan, bodyMenimbang, bodySection, bodyUntuk, bodyValue, dikeluarkanTanggal, dikeluarkanTempat, kopSection, tandaTangan, tembusan, titleSection } from './docV2';

const PrintDocument = () => {
    const pdfRef = useRef(null)

    const doc = new jsPDF({
        orientation:'portrait',
        unit:'px'
    });


    const generateJSPdf = () =>{
        const content = pdfRef.current;

        doc.setFontSize(12)
        doc.html(content, {
            callback: function (doc) {
                doc.save('a4');
            },
            html2canvas:{scale:0.33},
            margin:[40, 30, 40, 40]// left, top, right, bottom
            
        });

    };


    const generateDocx = () =>{
        const doc = new Document({
            numbering: {
                config: [
                    {
                        reference: "my-crazy-numbering",
                        levels: [
                            {
                                level: 0,
                                format: "decimal",
                                text: "%1.",
                                alignment: AlignmentType.START,
                                style: {
                                    paragraph: {
                                        indent: { left: 720, hanging: 260 },
                                    },
                                },
                            },
                            {
                                level: 1,
                                format: "decimal",
                                text: "%2.",
                                alignment: AlignmentType.START,
                                style: {
                                    paragraph: {
                                        indent: { left: 1440, hanging: 980 },
                                    },
                                },
                            },
                        ],
                    },
                ],
            },
            sections: [{
                headers:{
                    default:new Header({
                        children:[
                            new Paragraph({
                                alignment:AlignmentType.CENTER,
                                children:[
                                    new TextRun({
                                        text:'RAHASIA',
                                        size :22,
                                        color:'c7c7c7'
                                    })
                                ]
                            })
                        ]
                    })
                },
                    children:[
                        titleSection(),
                        new Paragraph({
                            spacing:{
                                after:200
                            }
                        }),
                        ...kopSection(),
                        new Paragraph({
                            spacing: {
                                after: 600
                            }
                        }),
                        bodyMenimbang(),
                        new Paragraph({
                            spacing: {
                                after: 200
                            }
                        }),
                        bodyDasar(),
                        new Paragraph({
                            spacing: {
                                after: 200
                            }
                        }),
                        ...bodyMemerintahkan(),
                        new Paragraph({
                            spacing: {
                                after: 200
                            }
                        }),
                        bodyUntuk(),
                        new Paragraph({
                            spacing: {
                                after: 400
                            }
                        }),
                        dikeluarkanTempat(),
                        new Paragraph({
                            spacing: {
                                after: 10
                            }
                        }),
                        dikeluarkanTanggal(),
                        new Paragraph({
                            spacing: {
                                after: 400
                            }
                        }),
                        ...tandaTangan(),
                        new Paragraph({
                            spacing: {
                                after: 400
                            }
                        }),
                        tembusan()
                        
                    ]
                
            }]
        })
        Packer.toBlob(doc).then((blob)=>{
            saveAs(blob, "example.docx");
        })
    };




  return (
    <div className='px-4 my-5' >
        <div className='row ' >
            <div className='col-9 border' >
                <div className='px-5 my-3' ref={pdfRef}>
                    <p className='h6 text-center text-muted'>RAHASIA</p>
                    <div className='d-flex justify-content-between'>
                        <p className='h6 text-start fw-bold'><u>KEJAKSAAN AGUNG R.I</u></p>
                        <p className=''>IN.1</p>
                    </div>
                    <div className='d-flex justify-content-end '>
                        <div style={{width : '12%'}}>
                            <p className='text-end'>COPY KE: 50 DARI 50 COPIES</p>
                        </div>
                    </div>
                    <p className='text-center'>SURAT PERINTAH TUGAS</p>
                    <p className='text-center'>................................................................................................</p>
                    <p className='text-center'>No : SP.TUG-13/KAR/{`[kodemasalah]`}/2/2023  </p>
                    <div className='row'>
                        <div className='col-2'>
                            <p>Menimbang</p>
                        </div>
                        <div className='col-1'>
                            : 1.
                        </div>
                        <div className='col-9'>
                            <p>{bodyValue}</p>.
                        </div>
                    </div>
                    <div className='row'>
                        <div className='col-2'>
                            <p>Dasar</p>
                        </div>
                        <div className='col-1'>
                            : 1.
                        </div>
                        <div className='col-9'>
                            <p>{bodyValue}</p>.
                        </div>
                    </div>
                    <p className='text-center'>MEMERINTAHKAN :</p>
                    <div className='row'>
                        <div className='col-2'>
                            <p>Kepada</p>
                        </div>
                        <div className='col-1'>
                            : 1.
                        </div>
                        <div className='col-9'>
                            <div className='row'>
                                <div className='col-2'>
                                    <p>Nama</p>
                                </div>
                                <div className='col-10'>
                                    <p>: AMATE ADMIN OTP</p>
                                </div>
                            </div>
                            <div className='row'>
                                <div className='col-2'>
                                    <p>Pangkat</p>
                                </div>
                                <div className='col-10'>
                                    <p>: IV/c</p>
                                </div>
                            </div>
                            <div className='row'>
                                <div className='col-2'>
                                    <p>NIP</p>
                                </div>
                                <div className='col-10'>
                                    <p>: 76544321</p>
                                </div>
                            </div>
                            <div className='row'>
                                <div className='col-2'>
                                    <p>Jabatan</p>
                                </div>
                                <div className='col-10'>
                                    <p>: Kepala Seksi Intelijen</p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div className='row'>
                        <div className='col-2'>
                            <p>Untuk</p>
                        </div>
                        <div className='col-1'>
                            : 1.
                        </div>
                        <div className='col-9'>
                            <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Officia dolorem laboriosam, ab fugit iure repellendus molestiae doloremque commodi corrupti unde obcaecati saepe, pariatur itaque. Consequuntur at aut velit doloribus esse.</p>.
                        </div>
                    </div>
                    <div className='row'>
                        <div className='col-7'></div>
                            <div className='col-2'>
                                <p>Dikeluarkan di</p>
                            </div>
                            <div className='col-3'>
                                <p>: Jakarta</p>
                            </div>
                    </div>
                    <div className='row'>
                        <div className='col-7'></div>
                            <div className='col-2'>
                                <p>Pada Tanggal</p>
                            </div>
                            <div className='col-3'>
                                <p>: 25 April 2021</p>
                            </div>
                    </div>
                    <div className='row mt-5'>
                        <div className='col-8'></div>
                            <div className='col-4'>
                                <p>KEPALA SEKSI INTELIJEN</p>
                            </div>
                    </div>
                    <div className='row mt-5'>
                        <div className='col-8'></div>
                            <div className='col-4'>
                                <p className='ms-3 mt-5 fw-bold'><u>AMATE ADMIN OTP</u></p>
                                <p >IV/c Pembina Utama Muda</p>
                                <p className='ms-5'>Nip:76554432</p>
                            </div>
                    </div>
                    <p>Tembusan :</p>
                    <p>123</p>
                </div>
            </div>
            <div className='col'>
                <div className='mb-3'>
                <button className='btn btn-primary' onClick={generateJSPdf}>PRINT Pdf</button>
                </div>
                <button className='btn btn-info' onClick={generateDocx}>PRINT DOCS</button>
            </div>
        </div>

    </div>
  )
}

export default PrintDocument